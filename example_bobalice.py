from ecc import Ecc
from tinyec import registry, ec
from Crypto.Cipher import ChaCha20_Poly1305
import secrets
import hashlib, binascii
import time

if __name__ == "__main__":
    security_level = 256

    print("------ Bob membuat kunci ------")
    t1 = time.perf_counter_ns()
    bob_curve = Ecc(security_level=security_level)
    bob_priv_key = bob_curve.generate_priv_key()
    bob_pub_key = bob_curve.calculate_pub_key(priv_key=bob_priv_key)
    t2 = time.perf_counter_ns()
    print(f'Bob Public Key: {bob_curve.get_pub_key_str(bob_pub_key)}')
    print(f"Waktu pembuatan kunci: {(t2-t1)/pow(10,9)} s\n")

    print("------ Alice membuat kunci ------")
    t1_alice = time.perf_counter_ns()
    alice_curve = Ecc(security_level=security_level)
    alice_priv_key = alice_curve.generate_priv_key()
    alice_pub_key = alice_curve.calculate_pub_key(priv_key=alice_priv_key)
    t2_alice = time.perf_counter_ns()
    print(f'Alice Public Key: {alice_curve.get_pub_key_str(alice_pub_key)}')
    print(f"Waktu pembuatan kunci: {(t2_alice-t1_alice)/pow(10,9)} s\n")

    print("------ Kunci rahasia bersama ------")
    bob_shared_key = bob_curve.ecc_point_to_256_bit_key(bob_priv_key*alice_pub_key)
    alice_shared_key = alice_curve.ecc_point_to_256_bit_key(alice_priv_key*bob_pub_key)
    print(f"Kunci Rahasia Bob: {bob_shared_key}")
    print(f"Kunci Rahasia Alice: {alice_shared_key}")
    print(f"Cek kunci rahasia sama: {bob_shared_key == alice_shared_key}\n")

    print("------ Alice mengenkripsi pesan ------")
    msg = b'hello world'
    print("Pesan asli:", msg)
    t1_encrypt = time.perf_counter_ns()
    alice_shared_key = alice_curve.ecc_point_to_256_bit_key(alice_priv_key*bob_pub_key)
    ciphertext, nonce, header, tag = alice_curve.encrypt_aed(msg=msg, secret=alice_shared_key)
    t2_encrypt = time.perf_counter_ns()
    print(f"Pesan terenkripsi: {ciphertext}")
    print(f"Waktu Enkripsi: {(t2_encrypt-t1_encrypt)/pow(10,9)} s\n")

    print("------ Bob mendekripsi pesan ------")
    t1_decrypt = time.perf_counter_ns()
    bob_shared_key = bob_curve.ecc_point_to_256_bit_key(bob_priv_key*alice_pub_key)
    plaintext = bob_curve.decrypt_aed(ciphertext=ciphertext, nonce=nonce, tag=tag, secret=bob_shared_key)
    t2_decrypt = time.perf_counter_ns()
    print(f"Pesan terdekripsi: {plaintext}")
    print(f"Waktu Dekripsi: {(t2_decrypt-t1_decrypt)/pow(10,9)} s\n")