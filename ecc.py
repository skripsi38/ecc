from tinyec import registry, ec
from Crypto.Cipher import ChaCha20_Poly1305
import secrets
import hashlib, binascii

def compress(pubKey):
    return hex(pubKey.x) + hex(pubKey.y % 2)[2:]

class Ecc():
    curve: registry.ec.Curve
    sec_level = {
        80 : "secp192r1",
        112 : "secp224r1",
        128 : "secp256r1",
        192 : "secp384r1",
        256 : "secp521r1"
    }

    def __init__(self, security_level = 80) -> None:
        curve_type = self.sec_level.get(security_level)
        if curve_type == None :
            raise ValueError
        self.curve = registry.get_curve(curve_type)
    
    def get_pub_key_str(self, pubKey):
        s =hex(pubKey.x) + hex(pubKey.y % 2)[2:]
        return hashlib.sha256(s.encode('utf-8')).hexdigest()

    def custom_curve(self, a, b, field, name) -> None:
        self.curve = registry.ec.Curve(a=a, b=b, field=field, name=name)

    def generate_priv_key(self) -> int:
        return secrets.randbelow(self.curve.field.n)

    def calculate_pub_key(self, priv_key: int):
        return priv_key * self.curve.g
    
    def encrypt_aed(self, msg, secret):
        header = b'header'
        cipher = ChaCha20_Poly1305.new(key=secret)
        cipher.update(header)
        ciphertext, tag = cipher.encrypt_and_digest(msg)
        return (ciphertext, cipher.nonce, header, tag)

    def decrypt_aed(self, ciphertext, nonce, tag, secret):
        try:
            header = b'header'
            cipher = ChaCha20_Poly1305.new(key=secret, nonce=nonce)
            cipher.update(header)
            plaintext = cipher.decrypt_and_verify(ciphertext=ciphertext, received_mac_tag=tag)
            print(plaintext)
            return plaintext
        except (ValueError, KeyError):
            return 

    def ecc_point_to_256_bit_key(self, point):
        sha = hashlib.sha256(int.to_bytes(point.x, 256, 'big'))
        sha.update(int.to_bytes(point.y, 256, 'big'))
        return sha.digest()

    def encrypt(self, msg, pub_key):
        """
            return ciphertext, nonce, tag, pub_key, priv_key
        """
        priv_key = self.generate_priv_key()
        shared_secret = priv_key*pub_key
        shared_secret_key = self.ecc_point_to_256_bit_key(shared_secret)
        ciphertext, nonce, header, tag = self.encrypt_aed(msg, shared_secret_key)
        pub_key = self.calculate_pub_key(priv_key=priv_key)
        return (ciphertext, nonce, tag, pub_key, priv_key)

    def decrypt(self, ecrypted_msg, priv_key):
        (ciphertext, nonce, tag, pub_key, p) = ecrypted_msg
        shared_secret = priv_key * pub_key
        shared_secret_key = self.ecc_point_to_256_bit_key(shared_secret)
        plaintext = self.decrypt_aed(ciphertext=ciphertext, nonce=nonce, tag=tag, secret=shared_secret_key)
        return plaintext